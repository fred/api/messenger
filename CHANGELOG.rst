ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

1.3.0 (2022-05-24)
------------------

* Update reference type (#3). Deprecate ``ReferenceType`` enum.

1.2.4 (2022-04-11)
------------------

* Add a batch send for emails (#8).

1.2.3 (2022-04-05)
------------------

* Add ``multi_send`` method for emails (#7).
* Update project setup.
* Fix CHANGELOG.

1.2.2 (2022-03-30)
------------------

* Replace ``setup_requires`` with ``pyproject.toml``.

1.2.1 (2022-03-01)
------------------

* Fix cmake build for C++.

1.2.0 (2022-02-14)
------------------

* Rename country_code to country (#2).
* Expand letters API with templates (#1).
* Fix and improve template comments.
* Update project setup.
* Add cmake build for C++.

1.1.1 (2022-03-30)
------------------

* Replace ``setup_requires`` with ``pyproject.toml``.

1.1.0 (2020-12-10)
------------------

* Change ``ListLetterRequest.recipients`` from ``Address`` to ``string``.

1.0.1 (2020-11-02)
------------------

* Add references to list requests.
* Add canceled status.
* Rename ``ChangeLog.rst`` to ``CHANGELOG.rst``.

1.0.0 (2020-10-12)
------------------

* Add the letter service.
* Improve comments of API.
* Add references and message types to API.

0.4.1 (2020-08-14)
------------------

* Fix broken import in SMS API.

0.4.0 (2020-08-11)
------------------

* Split common types to a separate file.
* Add the SMS service.

0.3.0 (2020-07-10)
------------------

* Add templates' UUID to Email message.

0.2.0 (2020-06-17)
------------------

* Add method to list emails.

0.1.0 (2020-06-16)
------------------

Initial version.

* Email sending and retrieving.
