==================
fred-api messenger
==================

Protocol buffers interface of messenger service for `FRED <https://fred.nic.cz/>`_ registry.

Python package
==============

Despite not having any Python modules, this repository contains ``setup.py``,
that takes care of generating Python modules from protocol buffers files during the build.
Therefore, this repository can be used as any other Python package.
