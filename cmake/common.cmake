if(DEFINED COMMON_CMAKE_CF4CE60F81004462ADD810242A9E6448)
    return()
endif()
set(COMMON_CMAKE_CF4CE60F81004462ADD810242A9E6448 1)

set(API_MESSENGER_VERSION "1.3.0")
message(STATUS "API_MESSENGER_VERSION: ${API_MESSENGER_VERSION}")

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")

set(PROTOS_DIR ${CMAKE_CURRENT_LIST_DIR}/..)
set(PROTOBUF_IMPORT_DIRS ${PROTOS_DIR})
set(FRED_MESSENGER_GRPC_FILES
    ${PROTOS_DIR}/fred_api/messenger/service_email_grpc.proto
    ${PROTOS_DIR}/fred_api/messenger/service_letter_grpc.proto
    ${PROTOS_DIR}/fred_api/messenger/service_sms_grpc.proto)

set(FRED_MESSENGER_PROTO_FILES
    ${PROTOS_DIR}/fred_api/messenger/common_types.proto
    ${PROTOS_DIR}/fred_api/messenger/email.proto
    ${PROTOS_DIR}/fred_api/messenger/letter.proto
    ${PROTOS_DIR}/fred_api/messenger/sms.proto
    ${FRED_MESSENGER_GRPC_FILES})

set(FRED_MESSENGER_GENERATE_INTO ${CMAKE_CURRENT_BINARY_DIR})
