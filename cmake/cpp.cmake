if(DEFINED CPP_CMAKE_217F4EEA277746D19940EF023ADD7D93)
    return()
endif()
set(CPP_CMAKE_217F4EEA277746D19940EF023ADD7D93 1)

include("${CMAKE_CURRENT_LIST_DIR}/common.cmake")

find_package(ProtobufCpp REQUIRED)
find_package(GrpcCpp REQUIRED)

include_directories(${PROTOBUF_INCLUDE_DIR} ${GRPC_INCLUDE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

if(CMAKE_CXX_STANDARD LESS 11)
    message(FATAL_ERROR "This file requires compiler and library support for the ISO C++ 2011 standard. This support must be enabled with the -std=c++11 or -std=gnu++11 compiler options. Set CMAKE_CXX_STANDARD to at least 11")
endif()

protobuf_generate_cpp(FRED_MESSENGER_PROTO_SRCS FRED_MESSENGER_PROTO_HDRS ${FRED_MESSENGER_GENERATE_INTO} ${FRED_MESSENGER_PROTO_FILES})
grpc_generate_cpp(FRED_MESSENGER_GRPC_SRCS FRED_MESSENGER_GRPC_HDRS ${FRED_MESSENGER_GENERATE_INTO} ${FRED_MESSENGER_GRPC_FILES})
