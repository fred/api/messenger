syntax = "proto3";

import "fred_api/messenger/common_types.proto";
import "google/protobuf/struct.proto";
import "google/protobuf/timestamp.proto";

package Fred.Messenger.Api.Email;

/*
RFC 2822 Message-ID
*/
message MessageId
{
    string value = 1;
}

/*
Email message itself
*/
message Email
{
    // If sender is not defined, default sender is used.
    // Valid value contains one or more email addresses, see https://tools.ietf.org/html/rfc5322#section-3.6.2.
    string sender = 1;
    // Required. Valid value contains one or more email addresses.
    // For details see section 3.4.1 "Addr-spec specification" of RFC2822,
    // https://tools.ietf.org/html/rfc2822#section-3.4.1.
    string recipient = 2;
    // Additional email headers, see https://tools.ietf.org/html/rfc5322#section-2.2.
    map<string, string> extra_headers = 3;
    // Message type - optional descriptive parameter.
    string type = 12;
    // Name of the templates for subject and body in text and HTML versions.
    // Subject and body templates are required.
    //
    // If template UUIDs are provided, respective template names are ignored and play only a descriptive role.
    string subject_template = 4;
    string body_template = 5;
    string body_template_html = 6;
    // A context used to render message parts from templates.
    google.protobuf.Struct context = 7;
    // List of attachment UIDs
    repeated Uid attachments = 8;
    // UUID of the templates for subject and body in text and HTML versions.
    //
    // If UUIDs are provided, template names play only a descriptive role.
    // In such case, the client is responsible for using matching template names and UUIDs.
    Uuid subject_template_uuid = 9;
    Uuid body_template_uuid = 10;
    Uuid body_template_html_uuid = 11;
}

/*
Complete email message and its metadata
*/
message EmailEnvelope
{
    // Email message unique identifier
    Uid uid = 1;
    google.protobuf.Timestamp create_datetime = 2;
    google.protobuf.Timestamp send_datetime = 3;
    google.protobuf.Timestamp delivery_datetime = 4;
    int32 attempts = 5;
    MessageStatus status = 6;
    Email message_data = 7;
    // Whether the message is or will be archived.
    bool archive = 8;
    // References to related objects.
    repeated Reference references = 9;
    // RFC 2822 Message-ID
    MessageId message_id = 16;
}
